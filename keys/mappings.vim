nnoremap <C-n> :NERDTreeToggle<CR>
nmap <Leader>s <Plug>(easymotion-s2)

nmap <C-s> :w<CR>
nmap <C-q> :q<CR>
